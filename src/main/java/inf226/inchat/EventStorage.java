package inf226.inchat;

import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        
        /*String sql =  "INSERT INTO Event VALUES('" + stored.identity + "','"
                                                  + stored.version  + "','"
                                                  + event.channel + "','"
                                                  + event.type.code + "','"
                                                  + event.time  + "')";
        */
        String sql =  "INSERT INTO Event VALUES(?,?,?,?,?)";
        PreparedStatement saevprepareStatement = connection.prepareStatement(sql);
        saevprepareStatement.setObject(1, stored.identity);
        saevprepareStatement.setObject(2, stored.version);
        saevprepareStatement.setObject(3, event.channel);
        saevprepareStatement.setObject(4, event.type.code);
        saevprepareStatement.setObject(5, event.time);
        saevprepareStatement.executeUpdate();
        
        //connection.createStatement().executeUpdate(sql);
    	PreparedStatement mesprepareStatement = connection.prepareStatement(sql);
        switch (event.type) {
            case message:
                /*sql = "INSERT INTO Message VALUES('" + stored.identity + "','"
                                                     + event.sender + "','"
                                                     + event.message +"')";
                */
            	sql = "INSERT INTO Message VALUES(?,?,?)";
            	mesprepareStatement = connection.prepareStatement(sql);
            	mesprepareStatement.setObject(1, stored.identity);
            	mesprepareStatement.setString(2, event.sender);
            	mesprepareStatement.setString(3, event.message);
            	mesprepareStatement.executeUpdate();
            	
                break;
            case join:
                /*sql = "INSERT INTO Joined VALUES('" + stored.identity + "','"
                                                  + event.sender +"')";
                */
            	sql = "INSERT INTO Joined VALUES(?,?)";
            	mesprepareStatement = connection.prepareStatement(sql);
            	mesprepareStatement.setObject(1, stored.identity);
            	mesprepareStatement.setString(2, event.sender);
            	mesprepareStatement.executeUpdate();
                break;
        }
        connection.createStatement().executeUpdate(sql);
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        /* String sql = "UPDATE Event SET" +
            " (version,channel,time,type) =('" 
                            + updated.version  + "','"
                            + new_event.channel  + "','"
                            + new_event.time  + "','"
                            + new_event.type.code
                            + "') WHERE id='"+ updated.identity + "'";
        */
    	String sql = "UPDATE Event SET(version,channel,time,type) = (?,?,?,?) WHERE id=?";
    	PreparedStatement upevprepareStatement = connection.prepareStatement(sql);
    	upevprepareStatement.setObject(1, updated.version);
    	upevprepareStatement.setObject(2, new_event.channel);
    	upevprepareStatement.setObject(3, new_event.time);
    	upevprepareStatement.setInt(4, new_event.type.code);
    	upevprepareStatement.setObject(5, updated.identity);
    	upevprepareStatement.executeUpdate();
    	
        //connection.createStatement().executeUpdate(sql);
    	PreparedStatement messprepareStatement = connection.prepareStatement(sql);
        switch (new_event.type) {
            case message:
                //sql = "UPDATE Message SET (sender,content)=('" + new_event.sender + "','"
                //                                     + new_event.message +"') WHERE id='"+ updated.identity + "'";
            	sql = "UPDATE Message SET (sender,content)=(?,?) WHERE id= ?";
            	messprepareStatement = connection.prepareStatement(sql);
            	messprepareStatement.setString(1, new_event.sender);
            	messprepareStatement.setString(2, new_event.message);
            	messprepareStatement.setObject(3, updated.identity);
            	messprepareStatement.executeUpdate();
                
                break;
            case join:
                //sql = "UPDATE Joined SET (sender)=('" + new_event.sender +"') WHERE id='"+ updated.identity + "'";
            	sql = "UPDATE Joined SET (sender)=(?) WHERE id=?";
            	messprepareStatement = connection.prepareStatement(sql);
            	messprepareStatement.setString(1, new_event.sender);
            	messprepareStatement.setObject(2, updated.identity);
            	messprepareStatement.executeUpdate();
            	
                break;
        }
        //connection.createStatement().executeUpdate(sql);
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
        //String sql =  "DELETE FROM Event WHERE id ='" + event.identity + "'";
        String sql =  "DELETE FROM Event WHERE id =?";
        PreparedStatement delevprepareStatement = connection.prepareStatement(sql);
        delevprepareStatement.setObject(1, event.identity);
        delevprepareStatement.executeUpdate();
        //connection.createStatement().executeUpdate(sql);
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        //final String sql = "SELECT version,channel,time,type FROM Event WHERE id = '" + id.toString() + "'";
    	final String sql = "SELECT version,channel,time,type FROM Event WHERE id =?";
    	PreparedStatement geevprepareStatement = connection.prepareStatement(sql);
    	geevprepareStatement.setString(1, id.toString());
        //final Statement statement = connection.createStatement();
        final ResultSet rs = geevprepareStatement.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            //final Statement mstatement = connection.createStatement();
            PreparedStatement gemessprepareStatement = connection.prepareStatement(sql);
            switch(type) {
                case message:
                    //final String msql = "SELECT sender,content FROM Message WHERE id = '" + id.toString() + "'";
                	final String msql = "SELECT sender,content FROM Message WHERE id =?";
                	gemessprepareStatement = connection.prepareStatement(msql);
                	gemessprepareStatement.setString(1, id.toString());
                    final ResultSet mrs = gemessprepareStatement.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    //final String asql = "SELECT sender FROM Joined WHERE id = '" + id.toString() + "'";
                	final String asql = "SELECT sender FROM Joined WHERE id =?";
                    gemessprepareStatement = connection.prepareStatement(asql);
                    gemessprepareStatement.setString(1, id.toString());
                    final ResultSet ars = gemessprepareStatement.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 

package inf226.inchat;

/**
 * Immutable UserName class
 */

public final class UserName {
	
	//Variables
	private final String userName;
	
	//getter
	public String getUserName() {
		return userName;
	}

	//constructor
	public UserName(String userName) {
		this.userName=userName;
	}
}

package inf226.inchat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;



/**
 * The UserStore stores User objects in a SQL database.
 */
public final class UserStorage
    implements Storage<User,SQLException> {
    
    final Connection connection;
    
    public UserStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS User (id TEXT PRIMARY KEY, version TEXT, name TEXT, joined TEXT)");
    }
    
    @Override
    public Stored<User> save(User user)
      throws SQLException {
        final Stored<User> stored = new Stored<User>(user);
        /*String sql =  "INSERT INTO User VALUES('" + stored.identity + "','"
                                                  + stored.version  + "','"
                                                  + user.name  + "','"
                                                  + user.joined.toString() + "')";
        */
        String sql =  "INSERT INTO User VALUES(?,?,?,?)";
        PreparedStatement sauserprepareStatement = connection.prepareStatement(sql);
        sauserprepareStatement.setObject(1, stored.identity);
        sauserprepareStatement.setObject(2, stored.version);
        sauserprepareStatement.setString(3, user.name);
        sauserprepareStatement.setString(3, user.joined.toString());
        sauserprepareStatement.executeUpdate();
        
        //connection.createStatement().executeUpdate(sql);
        return stored;
    }
    
    @Override
    public synchronized Stored<User> update(Stored<User> user,
                                            User new_user)
        throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<User> current = get(user.identity);
        final Stored<User> updated = current.newVersion(new_user);
        if(current.version.equals(user.version)) {
        /*String sql = "UPDATE User SET" +
            " (version,name,joined) =('" 
                            + updated.version  + "','"
                            + new_user.name  + "','"
                            + new_user.joined.toString()
                            + "') WHERE id='"+ updated.identity + "'";
        */
        String sql = "UPDATE User SET(version,name,joined) = (?,?,?) WHERE id=?";
        PreparedStatement upusprepareStatement = connection.prepareStatement(sql);
        upusprepareStatement.setObject(1, updated.version);
        upusprepareStatement.setString(2, new_user.name);
        upusprepareStatement.setString(3, new_user.joined.toString());
        upusprepareStatement.setObject(4, updated.identity);
        upusprepareStatement.executeUpdate();
        	
        	//connection.createStatement().executeUpdate(sql);
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<User> user)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<User> current = get(user.identity);
        if(current.version.equals(user.version)) {
            //String sql =  "DELETE FROM User WHERE id ='" + user.identity + "'";
        	String sql =  "DELETE FROM User WHERE id =?";
        	PreparedStatement deluserprepareStatement = connection.prepareStatement(sql);
        	deluserprepareStatement.setObject(1, user.identity);
        	deluserprepareStatement.executeUpdate();
            //connection.createStatement().executeUpdate(sql);
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<User> get(UUID id)
      throws DeletedException,
             SQLException {
        //final String sql = "SELECT version,name,joined FROM User WHERE id = '" + id.toString() + "'";
    	final String sql = "SELECT version,name,joined FROM User WHERE id =?";
    	PreparedStatement getuserprepareStatement = connection.prepareStatement(sql);
    	getuserprepareStatement.setString(1, id.toString());
        //final Statement statement = connection.createStatement();
        final ResultSet rs = getuserprepareStatement.executeQuery();

        if(rs.next()) {
            final UUID version = 
                UUID.fromString(rs.getString("version"));
            final String name = rs.getString("name");
            final Instant joined = Instant.parse(rs.getString("joined"));
            return (new Stored<User>
                        (new User(name,joined),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up a user by their username;
     **/
    public Maybe<Stored<User>> lookup(String name) {
        //final String sql = "SELECT id FROM User WHERE name = '" + name + "'";
    	final String sql = "SELECT id FROM User WHERE name =?";
    	
        try{
        	PreparedStatement luuserprepareStatement = connection.prepareStatement(sql);
        	luuserprepareStatement.setString(1, name);
        	
            //final Statement statement = connection.createStatement();
            final ResultSet rs = luuserprepareStatement.executeQuery();
            if(rs.next())
                return Maybe.just(
                    get(UUID.fromString(rs.getString("id"))));
        } catch (Exception e) {
        
        }
        return Maybe.nothing();
    }
}



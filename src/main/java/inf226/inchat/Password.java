package inf226.inchat;

/*
 * Immutable Password class
 */

public final class Password {
	
	//Variables
	private String password;
	
	//getter
	public String getPassword() {
		return password;
	}
	
	//checking for length grater than 8
	private boolean checkPassword(String password) {
		return password.length() >= 8;
	}
	
	//constructor
	public Password(String password) throws IllegalAccessException{
		if (checkPassword(password)) {
			this.password = password;
		} else {
			throw new IllegalAccessException("Password must be minimum 8 character");
		}
	}
}
